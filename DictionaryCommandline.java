package DictionaryProgram;

/**
* DictionaryCommandline la chuong trinh xu ly cac yeu cau
* @author Dat va HaiDB :v
* @version 1.0
* @since 2018 - 09 - 25
*/

import java.util.*;
import java.io.Console;

public class DictionaryCommandline
{
	
	public void showAllWords(DictionaryManagement inp_list)
	{
		System.out.println();
		System.out.println("Cac tu trong danh sach la: ");
		for (Word w : inp_list.dic_list)
		{
			System.out.println(w.word_target + "\t" + w.word_explain);
		}
	}

	public void dictionaryBasic(DictionaryManagement inp_list)
	{
		inp_list.insertFromCommandline();
		showAllWords(inp_list);
	}

	public void dictionaryAdvanced(DictionaryManagement inp_list)
	{
		inp_list.insertFromFile();
		showAllWords(inp_list);

		int check_lookup = inp_list.dictionaryLookup("tree");
		if (check_lookup != -1)
			System.out.println(check_lookup);
		else
			System.out.println("Khong tim thay tu !");
	}

	public int getChoice()
	{
		System.out.println("1. Tra tu.");
		System.out.println("2. Them tu.");
		System.out.println("3. Xoa tu.");
		System.out.println("4. Xem tat ca tu.");
		System.out.println("5. Xuat tat ca tu ra file.");
		System.out.println("6. Thoat.");

		System.out.println();
		System.out.printf("Lua chon cua ban la: ");
		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt();
		return choice;
	}

	public void dictionaryRun(DictionaryManagement inp_list)
	{
		boolean dictionaryRunning = true;
		int _choice = 0;
		while (dictionaryRunning && _choice==0)
		{
			System.out.println();
			_choice = this.getChoice();

			if (_choice == 1)
			{
				System.out.printf("Nhap tu can tra: ");
				String find_word = inp_list.insertFromCommandline();
				//System.out.println("Word need to find: " + find_word);
				String explain_of_find_word = inp_list.getExplainOfWord(find_word);				
				System.out.println("Meaning: " + explain_of_find_word);
			}
			if (_choice == 2)
			{
				System.out.printf("Tu can them: ");
				String add_target_word = inp_list.insertFromCommandline();
				System.out.printf("Nghia cua tu: ");
				String add_explain_word = inp_list.insertFromCommandline();
				Word add_word = new Word(add_target_word, add_explain_word);
				inp_list.addWord(add_word);
			}
			if (_choice == 3)
			{
				System.out.printf("Tu can xoa: ");
				String delete_word = inp_list.insertFromCommandline();
				inp_list.removeWord(delete_word);
				System.out.println("Da xoa tu: " + delete_word);
			}
			if (_choice == 4)
			{
				this.showAllWords(inp_list);
			}
			if (_choice ==  5)
			{
				inp_list.dictionaryExportToFile();
			}
			if (_choice == 6)
			{
				dictionaryRunning = false;
			}
			_choice = 0;
		}
	}
	public static void main(String[] args) 
	{
		DictionaryManagement dic_Mana = new DictionaryManagement();
		DictionaryCommandline dic_command = new DictionaryCommandline();
		dic_command.dictionaryAdvanced(dic_Mana);

		/*
		System.out.println("\n***Start Remove***\n");
		dic_Mana.removeWord("car");
		dic_Mana.removeWord("book");
		dic_command.showAllWords(dic_Mana);

		System.out.println("\n***Start Add***\n");
		dic_Mana.addWord(new Word("car","O to"));
		dic_Mana.addWord(new Word("book","Quyen sach"));
		dic_command.showAllWords(dic_Mana);
		*/
		dic_command.dictionaryRun(dic_Mana);
	}
}