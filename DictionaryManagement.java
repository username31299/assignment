package DictionaryProgram;

/** 
* DictionaryManagement la chuong trinh quan ly tu dien
* @author Dat va HaiDB :v
* @version 1.0
* @since 2018 - 09 - 25
*/

import java.util.*;
import java.io.*;

public class DictionaryManagement
{
	public TreeSet<Word> dic_list = 
		new TreeSet<Word>();

	public String insertFromCommandline()
	{
		Scanner sc = new Scanner(System.in);
		String res = sc.next();
		return res;
	}

	public void insertFromFile()
	{
		final String filename = "dictionaries.txt";
		String line = null;
		
		try
		{
			FileReader fileReader = 
				new FileReader(filename);

			BufferedReader fin = 
				new BufferedReader(fileReader);

			while ((line = fin.readLine()) != null)
			{
				//System.out.println(line);
				int indexOfTab = line.indexOf("\t",0);

				//System.out.println(indexOfTab);
				Word temp_word = new Word();

				temp_word.word_target = line.substring(0,indexOfTab);
				temp_word.word_explain = line.substring(indexOfTab+1,line.length());

				dic_list.add(temp_word);
			}

			// Always close files.
			fin.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public void dictionaryExportToFile()
	{
		final String filename = "new_dictionaries.txt";

		try
		{
			FileWriter fileWriter = 
				new FileWriter(filename);

			BufferedWriter fout =
				new BufferedWriter(fileWriter);

			for (Word w : dic_list)
			{
				fout.append(w.word_target+"\t"+w.word_explain+"\n");
			}

			fout.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		System.out.println("Da xuat tat ca ra file. ");
	}

	public int dictionaryLookup(String wordLooking)
	{
		int indexFinding = -1;
		boolean checkFinding = false;
		for (Word w : dic_list)
		{
			indexFinding++;
			if (wordLooking.equals(w.word_target))
			{
				return indexFinding;
			}
		}
			
		return -1;
	}

	public String getExplainOfWord(String wordLooking)
	{
		int indexExplainWord = this.dictionaryLookup(wordLooking);
		//System.out.println("indexExplainWord: " + indexExplainWord);
		String explain_word = "";
		if (indexExplainWord == -1)
		{
			explain_word = "Khong tim thay tu !";
		}
		else
		{
			explain_word = new ArrayList<>(dic_list).get(indexExplainWord).word_explain;
		}

		return explain_word;
	}

	public void removeWord(String remove_word)
	{	
		int indexRemove = dictionaryLookup(remove_word);
		
		if (indexRemove != -1)
		{
			Word r_word = new ArrayList<>(dic_list).get(indexRemove);
			dic_list.remove(r_word);
		}
	}

	public void addWord(Word w)
	{
		dic_list.add(w);
	}
	
}
